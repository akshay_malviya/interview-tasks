# Task manager

### You have to develop a general purpose task manager

**Use cases:**

- As admin
    - Admin can add a member
    - Admin can view list of all the tasks
    - Admin can view information of an individual task
    - Admin can add and remove task, for a particular member or himself
    - Admin can mark a task, todo or done, for a particular member or himself
    - Admin can apply filters using name, description, member, project and tags
- As member
    - Member can view the list of tasks, assigned to him
    - Member can view information of an individual task
    - Member can add or remove tasks for himself
    - Member can mark a task
    - Member can apply filters using name, description, project, tags

**A task must have following fields:**

- Name
- Description
- List of subtasks, that can be marked done or todo
- A member it is assigned to
- A project it belongs to
- Tags

**A member must have following fields:**

- Name
- Contact Number

**A project must have the following fields:**

- Name
- Description

**Implementation requirements**

- The app should work in SSR mode
- Persist app state using local storage
- Create views as seems fit, like list of all the members, projects or login, signup views

**Languages & technologies:**

- HTML
- CSS or SCSS
- JavaScript
- Nuxt JS
- Element UI

**Note:** You can use any other alternative technologies like react, next and mui

**Extras:**

- You can design the ui and ux the way you want

